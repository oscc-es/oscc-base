const options = {month: 'long', day: 'numeric', year: 'numeric'};

module.exports = (isoString, locale = 'es-ES') => {
  const date = new Date(isoString);
  const longDate = new Intl.DateTimeFormat(locale, options).format(date);
  return longDate;
};
