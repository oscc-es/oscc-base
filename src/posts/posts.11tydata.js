let data = {
  layout: 'layouts/post.njk',
  permalink: '/blog/{{ page.fileSlug }}/index.html',
  date: 'Last Modified'
};

if (process.env.NODE_ENV === 'production') {
  data.date = 'git Last Modified';
}

module.exports = data;
